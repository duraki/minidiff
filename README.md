# minidiff
minidiff is git-diff inspired PHP library for difference algorithm

# algorithm
This library tries to solve an common problem of finding a longest common
subsequence of two sequences A and B and a shortest edit script for transforming
A into B.  
  
Instead of using Wagner & Fischer early algorithm of `O(n^2)` we may use `O(nd)`
where `n` is the **sum of the lengths of A and B**, and `d` is the size of the
minimum edit script for A and B. Under a basic stochastic model, the algorithm
is shown to have `O(n + d^2)` expected-time performance. As noted by Eugene W.
Myers, the algorithm that use `less than - equal - greater than` comprasions
described as `Ω(NlgN)` is the best lower bound known.

Unlike others, this algorithm uses the **greedy** design paradigm and exposes the
relationship of the longest common subsequence problem to the single-source
shortest path problem.

# minidiff algorithm
Usage statistics of transoforming A into B diff is using advantage-sequence
based on Unix `grep` command. If two presented buffers are compared each with
other, we may start with lines' `strcmp()` first between line buffers, and
regarding wether the diff is presented, compare by 5 characters in regular, and
10 characters in fast mode.
  
Given the two line buffers:  
  
    * buff_1 `= 'there is a difference'`  
    * buff_2 `= 'there is no difference'`  
  


The sequence of the given charsums are equal to:    
  
    * `is? buff_1 == buff_2` ->  
        * -> yes -> **No diff**  
        * -> no -> **Diff presented** ->  
            * -> `is? there == there`;   
            * -> `is? .is.a == .is.n`  
  
Such algorithm is faster then comparing char by char. This returns a given
analyed output between buffers.
